# ~/ABRÜPT/MARCEL DEBURAS/LA FIN DES TEMPS NE VIENT PAS/*

La [page de ce livre](https://abrupt.ch/marcel-deburas/la-fin-des-temps-ne-vient-pas/) sur le réseau.

## Sur le livre

« L’homme n’est pas un loup pour l’homme, il est une tombe. » L’usine ferme ses portes, et son spectacle, et ses poussières d’homme qui volettent insensément, qui se meurent souvent, renaissent en d’autres oppressions. Une prose fragmentée, sans entame ni fin, qui gratte la rouille, quête la dialectique.

## Sur l'auteur

Marcel Deburas. Né à Paris. 1988. Paysagiste. Trieste, maintenant. Étude de la ruine en tant que construction du paysage. Parallèlement à ses recherches théoriques, la littérature. Comme un souffle depuis l’ordinaire.

## Sur la licence

Cet [antilivre](https://abrupt.ch/antilivre/) est disponible sous licence Creative Commons Attribution – Pas d’Utilisation Commerciale – Partage dans les Mêmes Conditions 4.0 International (CC-BY-NC-SA 4.0).

## Etc.

Vous pouvez également découvrir notre [site](https://abrupt.ch) pour davantage d'informations sur notre démarche, notamment quant au [partage de nos textes](https://abrupt.ch/partage/).
